# mongoose-i18n

mongoose-i18n is a mongoose plugin to support i18n and localization in your mongoose schemas.

Fork: [fd9543f](https://github.com/rusty1s/mongoose-i18n-localize)

## Usage

```
yarn add bitbucket:forking/mongoose-i18n
```

Create your schema(Support nesting):

```js
let mongoose = require('mongoose')
let mongooseI18n = require('mongoose-i18n')

let BookSchema = new mongoose.Schema({
	name: {
		type: String,
		i18n: true
	},
	tags: [{
	  type: String,
	  i18n: true
	}],
	comments: {
		top: {
      type: String,
      i18n: true
    },
    all: [{
      type: String,
      i18n: true
    }]
	}
})

BookSchema.plugin(mongooseI18n, {
	locales: ['en', 'zh']
})

let Model = mongoose.model('Book', BookSchema)
```

This will create a structure like:

```js
{
	name: {
		en: String,
		zh: String
	},
	tags: {
	  en: [String],
	  zh: [String]
  },
  comments: {
		top: {
			en: String,
			zh: String
		},
    all: {
		  en: [String],
		  zh: [String]
	  }
	}
}
```

All validators of `name` get also assigned to `name.en` and `name.zh`.

mongoose-i18n adds the methods `toObjectLocalized(resource, locale)` and `toJSONLocalized(resource, locale)` to the i18n schema methods. To set the locale of a resource to `en`, just do:


```js
Model.find(function(err, resources) {
	let localizedResources = resources.toJSONLocalized('en')
})

//or

Model.find(function(err, resources) {
	let localizedResources = Model.schema.methods.toJSONLocalized(resources, 'en')
});
```

`localizedResources` has now the following structure:

```js
[
	{
		name: {
			en: 'hello',
			zh: '你好',
			localized: 'hello'
		},
		tags: {
			en: ['hello'],
			zh: ['你好'],
			localized: ['hello']
		}
	}
]
```

Use `toObjectLocalized` or `toJSONLocalized` according to `toObject` or `toJSON`.

If you only want to show only one locale message use the methods
`toObjectLocalizedOnly(resource, locale, localeDefault)` or
`toJSONLocalizedOnly(resource, locale, localeDefault)`.

# Tests

To run the tests you need a local MongoDB instance available. Run with:

```
yarn test
```
# Issues

Please use the Bitbucket issue tracker to raise any problems or feature requests.

If you would like to submit a pull request with any changes you make, please feel free!
